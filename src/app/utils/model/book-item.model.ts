export interface BookItem {
  img: string;
  name: string;
  author: string;
  company: string;
  type: string;
  rate: number;
  price: number;
}

export interface reqSuggest {
    keyword: string;
    type: number;
}

export interface BookCategory {
  type: number;
  name: string;
}