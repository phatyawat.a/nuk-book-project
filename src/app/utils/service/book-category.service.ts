import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { SharedService } from '../share.service';


const endpoint = environment.endPoint;
const port = environment.port[1];
const httpOptions = {
  headers: new HttpHeaders({
    'content-type': 'application/json;charset=UTF-8'
  }),
};
@Injectable({
  providedIn: 'root'
})
export class CategoryService {
  param = new HttpParams();
  constructor(
    private http: HttpClient,
    private shared:SharedService
  ) { }


  getBookCategory(): Observable<string[]> {
    const url = endpoint + port + "bookCats";
    console.log(url);
    return this.http.get<string[]>(url, httpOptions).pipe(
         tap(_ => console.log(url))
    );
  }


  //   editMerchantAddress(req: EditConsentRequest): Observable<ResponseApi<Object>> {
  //     const url = endpoint + '/edit';
  //     return this.http.post<ResponseApi<Object>>(url, req, httpOptions).pipe(
  //       tap(_ => console.log('insertConsent success'))
  //     );
  //   }
  
}

