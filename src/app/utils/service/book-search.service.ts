import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { reqSuggest, BookItem } from '../model/book-item.model';
import { SharedService } from '../share.service';


const endpoint = environment.endPoint;
const port = environment.port[0];
const httpOptions = {
  headers: new HttpHeaders({
    'content-type': 'application/json;charset=UTF-8'
  }),
};
@Injectable({
  providedIn: 'root'
})
export class BookSearchService {
  param = new HttpParams();
  constructor(
    private http: HttpClient,
    private shared:SharedService
  ) { }


  searchSuggest(req: reqSuggest): Observable<string[]> {
    const url = endpoint + port + "searchSuggest" + this.shared.convertObjectToQueryString(req);
    console.log(url);
    return this.http.get<string[]>(url, httpOptions).pipe(
         tap(_ => console.log(url))
    );
  }

  searchBook(req: reqSuggest): Observable<BookItem[]> {

    const url = endpoint + port + "searchBook" + this.shared.convertObjectToQueryString(req);   
    return this.http.get<BookItem[]>(url, httpOptions).pipe(
         tap(_ => console.log('syncCIFInfo'))
    );
  }

 

  //   editMerchantAddress(req: EditConsentRequest): Observable<ResponseApi<Object>> {
  //     const url = endpoint + '/edit';
  //     return this.http.post<ResponseApi<Object>>(url, req, httpOptions).pipe(
  //       tap(_ => console.log('insertConsent success'))
  //     );
  //   }
  
}
