import { Injectable } from '@angular/core';




@Injectable({
    providedIn: 'root'
})
export class SharedService {
    convertObjectToQueryString(values: any): string {
        if (!values) {
            return '';
        }

        const query = Object.keys(values)
            .map((key) => key + '=' + values[key])
            .join('&');

        return '?' + query;
    }
}