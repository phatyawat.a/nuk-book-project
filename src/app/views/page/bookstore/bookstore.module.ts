import { NgModule } from '@angular/core';
import { BookstoreRoutingModule } from './bookstore-routing.module';
import { BookstoreComponent } from './bookstore/bookstore.component';
import { SearchBooksComponent } from './bookstore/components/search-books/search-books.component';
import { AppSharedModule } from 'src/app/app-shared.module';
import { BookCardComponent } from './bookstore/components/book-card/book-card.component';
import {MatCardModule} from '@angular/material/card';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatFormFieldModule} from '@angular/material/form-field';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatSelectModule} from '@angular/material/select';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';

import { CommonModule } from '@angular/common';


@NgModule({
  declarations: [
    BookstoreComponent,
    SearchBooksComponent,
    BookCardComponent
  ],
  imports: [
    BookstoreRoutingModule,
    MatCardModule,
    MatSelectModule,
    MatAutocompleteModule,
    MatFormFieldModule,
    FormsModule,
    CommonModule,
    ReactiveFormsModule,
    MatIconModule,
    MatButtonModule,
    AppSharedModule
  ],

})

export class BookstoreModule { }
