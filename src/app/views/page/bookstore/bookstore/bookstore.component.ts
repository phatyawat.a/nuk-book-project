import { Component, OnInit } from '@angular/core';
import { CoreService } from '../../../../service/core';
import { TranslateService } from '@ngx-translate/core';
import { BookSearchService } from 'src/app/utils/service/book-search.service';
import { reqSuggest, BookItem } from 'src/app/utils/model/book-item.model';

@Component({
  selector: 'app-bookstore',
  templateUrl: './bookstore.component.html',
  styleUrls: ['./bookstore.component.css']
})
export class BookstoreComponent implements OnInit {

  bookList: BookItem[] = [];

  constructor(core: CoreService,
    private searchBook: BookSearchService) {
    core.setDefaultLang('th')

  }
  ngOnInit() {


  }

  setBookList(response: BookItem[]) {
    this.bookList = response;
  }

  counterRow(i: number) {
    
    let n: number =  Math.round(i / 5);
    let round = i%5
    
    if(round != 0){
     n = n+ 1;
    }
    
    return new Array(n);
  }

  counterCol(i: number) {
    let n: number = i;
    return new Array(n);
  }

  getItem(row:number,col:number):BookItem {
    let n = (row * 5)+ col; 
    let target = this.bookList[n];
    return target;
  }

}
