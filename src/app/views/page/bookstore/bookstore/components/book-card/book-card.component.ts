import { Component, Input, OnInit } from '@angular/core';
import { BookItem } from 'src/app/utils/model/book-item.model';

@Component({
  selector: 'app-book-card',
  templateUrl: './book-card.component.html',
  styleUrls: ['./book-card.component.css']
})
export class BookCardComponent implements OnInit {

  @Input() bookInput:BookItem = {
    img: "",
    name: "",
    author: "",
    company: "",
    type: "",
    rate: 0,
    price: 0
  }
  
  constructor() { }

  ngOnInit(): void {
  }

}
