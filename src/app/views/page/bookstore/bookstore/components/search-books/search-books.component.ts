import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { reqSuggest, BookItem, BookCategory } from 'src/app/utils/model/book-item.model';
import { BookSearchService } from 'src/app/utils/service/book-search.service';
import { CategoryService } from '../../../../../../utils/service/book-category.service';


@Component({
  selector: 'app-search-books',
  templateUrl: './search-books.component.html',
  styleUrls: ['./search-books.component.css']
})
export class SearchBooksComponent implements OnInit {

  @Output() onSearch = new EventEmitter<BookItem[]>()

  bookList: Array<string> = [];
  categoryList:Array<BookCategory> = [];
  myControl = new FormControl();
  options: string[] = ['One', 'Two', 'Three'];
  filteredOptions!: Observable<string[]>;

  reqParam: reqSuggest = {
    keyword: "",
    type: 0
  }

  categoryName:string = "";

  constructor(private searchBook: BookSearchService,
              private category:CategoryService) {
                this.getCategory()
              }

  ngOnInit() {
    this.getBook(this.reqParam);
    this.buildForm();
  }

  buildForm(){
    this.myControl.valueChanges.subscribe(x => {
      this.reqParam.keyword = this.myControl.value
      this.suggestBook(this.myControl.value)
   })
  }

  suggestBook(keyword:string) {
    
    if (keyword.length > 2) {
      this.getSuggest()
    }else{
      this.bookList = [];
    }

  }

  getCategory(){
    this.category.getBookCategory().subscribe(
      (response: any[]) => { 
        console.log(response);
             
          this.categoryList = response; 
          this.categoryName = this.categoryList[0].name;
      }
    );
  }

  getSuggest(){
    this.searchBook.searchSuggest(this.reqParam).subscribe(
      (response: string[]) => {      
          this.bookList = response;     
      }
    );
  }

  setType(cat:BookCategory) {      
    this.reqParam.type = cat.type;
    this.categoryName = cat.name;
    this.getBook(this.reqParam);
  }

  selectItem(keyword:any){
    let txt:string = keyword.target.innerText;
    this.reqParam.keyword = txt;
    this.getBook(this.reqParam);
  }

  getBook(req: reqSuggest){
    this.searchBook.searchBook(req).subscribe(
      (response: BookItem[]) => {      
        this.onSearch.emit(response)
      }
    );
  }

  


}
